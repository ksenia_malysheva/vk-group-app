'use strict';

describe('vkGroupApp Controllers', function () {

	beforeEach(function() {
		localStorage.clear()
	});
	describe('Group Details Controller', function () {
		var scope, ctrl, $httpBackend, $routeParams;
		var groupDetailResponse = '{"response":[219,{"id":111725,"from_id":-22822305,"to_id":-22822305,"date":1423662246,"post_type":"post","text":"День, который ждали. День, который отныне будет отмечаться в календарях красным цветом. День, когда вышли обновления VK App для iPhone и iPad.<br><br>Долгое время томительного ожидания не прошло даром: оба приложения обзавелись множеством новых функций. Так, в приложении для iPhone были расширены возможности отправки стикеров, появились новые функции для работы с профилями и стенами, а также были добавлены новые возможности поиска и добавления в друзья других пользователей — и это далеко не всё!<br><br>О VK App для iPad уже складывали легенды — и его обновление вышло не менее легендарным: совершенно новый дизайн и полная адаптация под iOS 8, все недавние ключевые обновления на сайте и более сотни других различных функций и изменений были добавлены в новую версию приложения. Полный обзор нового приложения вы можете прочесть на указанной ниже странице.<br><br>К сожалению, в связи с политикой Apple нам пришлось временно отказаться от музыкального раздела в приложениях ВКонтакте. Мы прикладываем все усилия, чтобы вернуть его в ближайшем будущем. А пока вы можете слушать музыку с любого устройства через мобильную версию ВКонтакте: m.vk.com\/audio.<br><br>Ознакомиться с обновлениями VK App и скачать приложения можно по ссылкам vk.cc\/iphone и vk.cc\/ipad. С обзором нововведений приложения для iPad вы можете ознакомиться здесь: vk.com\/page-2158488_49139418<br><br>#iOSupdate@team","attachment":{"type":"photo","photo":{"pid":356156329,"aid":-7,"owner_id":-22822305,"user_id":100,"src":"http:\/\/cs7010.vk.me\/v7010135\/133\/yciCmrr0Ogc.jpg","src_big":"http:\/\/cs7010.vk.me\/v7010135\/134\/ndwS_1E--vQ.jpg","src_small":"http:\/\/cs7010.vk.me\/v7010135\/132\/JKPXMp8g7Lk.jpg","src_xbig":"http:\/\/cs7010.vk.me\/v7010135\/135\/m6Opsyvswdo.jpg","width":790,"height":510,"text":"","created":1423662248,"access_key":"560c733674075b9da9"}},"attachments":[{"type":"photo","photo":{"pid":356156329,"aid":-7,"owner_id":-22822305,"user_id":100,"src":"http:\/\/cs7010.vk.me\/v7010135\/133\/yciCmrr0Ogc.jpg","src_big":"http:\/\/cs7010.vk.me\/v7010135\/134\/ndwS_1E--vQ.jpg","src_small":"http:\/\/cs7010.vk.me\/v7010135\/132\/JKPXMp8g7Lk.jpg","src_xbig":"http:\/\/cs7010.vk.me\/v7010135\/135\/m6Opsyvswdo.jpg","width":790,"height":510,"text":"","created":1423662248,"access_key":"560c733674075b9da9"}}],"comments":{"count":0},"likes":{"count":4304},"reposts":{"count":576}},{"id":108588,"from_id":-22822305,"to_id":-22822305,"date":1421407326,"post_type":"copy","text":"Новый рекорд посещаемости ВКонтакте установлен на этой неделе — 70 миллионов человек за сутки.","copy_post_date":1421406016,"copy_post_type":"post","copy_owner_id":6492,"copy_post_id":5069,"copy_text":"Спасибо, что вы с нами!","attachment":{"type":"photo","photo":{"pid":352920894,"aid":-7,"owner_id":6492,"src":"http:\/\/cs7010.vk.me\/c540106\/v540106492\/141d2\/E5bE_d3vBzo.jpg","src_big":"http:\/\/cs7010.vk.me\/c540106\/v540106492\/141d3\/KL02R_7iQuc.jpg","src_small":"http:\/\/cs7010.vk.me\/c540106\/v540106492\/141d1\/p-xpJ-rT5zA.jpg","src_xbig":"http:\/\/cs7010.vk.me\/c540106\/v540106492\/141d4\/6T7DslrYbEo.jpg","width":666,"height":588,"text":"","created":1421364474,"post_id":5069,"access_key":"ee6bdfe396589e4ce2"}},"attachments":[{"type":"photo","photo":{"pid":352920894,"aid":-7,"owner_id":6492,"src":"http:\/\/cs7010.vk.me\/c540106\/v540106492\/141d2\/E5bE_d3vBzo.jpg","src_big":"http:\/\/cs7010.vk.me\/c540106\/v540106492\/141d3\/KL02R_7iQuc.jpg","src_small":"http:\/\/cs7010.vk.me\/c540106\/v540106492\/141d1\/p-xpJ-rT5zA.jpg","src_xbig":"http:\/\/cs7010.vk.me\/c540106\/v540106492\/141d4\/6T7DslrYbEo.jpg","width":666,"height":588,"text":"","created":1421364474,"post_id":5069,"access_key":"ee6bdfe396589e4ce2"}}],"comments":{"count":0},"likes":{"count":20258},"reposts":{"count":1860}},{"id":106940,"from_id":-22822305,"to_id":-22822305,"date":1420050886,"post_type":"copy","text":"Год за годом ВКонтакте совершенствуется и растёт по всем показателям, и этот год не стал исключением. Мы собрали статистику по отправленным сообщениям, стикерам, подаркам, загруженным фотографиям и поставленным отметкам «Мне нравится». Полученные числа не перестают удивлять.<br><br>vk.com\/page-2158488_49214544","copy_post_date":1420033836,"copy_post_type":"post","copy_owner_id":-2158488,"copy_post_id":403185,"copy_text":"Совместно с коллегами из медиахолдинга [club2158488|LIVE] посчитали активность наших пользователей на сайте за прошедший год.","attachment":{"type":"photo","photo":{"pid":350619887,"aid":-7,"owner_id":-2158488,"user_id":100,"src":"http:\/\/cs7010.vk.me\/c540105\/v540105883\/1f266\/rERqks9Kdhw.jpg","src_big":"http:\/\/cs7010.vk.me\/c540105\/v540105883\/1f267\/k-rp5s29Ebk.jpg","src_small":"http:\/\/cs7010.vk.me\/c540105\/v540105883\/1f265\/B7M_KpdCDrE.jpg","src_xbig":"http:\/\/cs7010.vk.me\/c540105\/v540105883\/1f268\/mibuoVm6LFU.jpg","width":790,"height":511,"text":"","created":1420033839,"post_id":403185,"access_key":"7897ef15084c783a09"}},"attachments":[{"type":"photo","photo":{"pid":350619887,"aid":-7,"owner_id":-2158488,"user_id":100,"src":"http:\/\/cs7010.vk.me\/c540105\/v540105883\/1f266\/rERqks9Kdhw.jpg","src_big":"http:\/\/cs7010.vk.me\/c540105\/v540105883\/1f267\/k-rp5s29Ebk.jpg","src_small":"http:\/\/cs7010.vk.me\/c540105\/v540105883\/1f265\/B7M_KpdCDrE.jpg","src_xbig":"http:\/\/cs7010.vk.me\/c540105\/v540105883\/1f268\/mibuoVm6LFU.jpg","width":790,"height":511,"text":"","created":1420033839,"post_id":403185,"access_key":"7897ef15084c783a09"}},{"type":"page","page":{"pid":"49214544","gid":2158488,"title":"Статистика ВКонтакте 2014","view_url":"http:\/\/m.vk.com\/page-2158488_49214544?api_view=81c2654ff5437cf81ad38c2654fd75"}}],"comments":{"count":0},"likes":{"count":10292},"reposts":{"count":1333}},{"id":106894,"from_id":-22822305,"to_id":-22822305,"date":1420017433,"post_type":"post","text":"Подходит к концу 2014 год. Это были удивительные 365 дней, и мы благодарны за то, что вы разделили переживания этих дней с нами. На прикреплённой странице собраны самые обсуждаемые пользователями ВКонтакте события, люди и явления уходящего года, а также количество их упоминаний на сайте. Давайте ещё раз вспомним, каким был он, уходящий 2014.<br><br>#2014@team","attachment":{"type":"photo","photo":{"pid":351738206,"aid":-7,"owner_id":-22822305,"user_id":100,"src":"http:\/\/cs7010.vk.me\/c540105\/v540105420\/16e94\/bRYbQmXd_4o.jpg","src_big":"http:\/\/cs7010.vk.me\/c540105\/v540105420\/16e95\/3RxXs1K7KNo.jpg","src_small":"http:\/\/cs7010.vk.me\/c540105\/v540105420\/16e93\/2pktvD0zvxk.jpg","src_xbig":"http:\/\/cs7010.vk.me\/c540105\/v540105420\/16e96\/C1djz_APdsQ.jpg","width":790,"height":511,"text":"","created":1420008565,"access_key":"d58af84c6abcdea676"}},"attachments":[{"type":"photo","photo":{"pid":351738206,"aid":-7,"owner_id":-22822305,"user_id":100,"src":"http:\/\/cs7010.vk.me\/c540105\/v540105420\/16e94\/bRYbQmXd_4o.jpg","src_big":"http:\/\/cs7010.vk.me\/c540105\/v540105420\/16e95\/3RxXs1K7KNo.jpg","src_small":"http:\/\/cs7010.vk.me\/c540105\/v540105420\/16e93\/2pktvD0zvxk.jpg","src_xbig":"http:\/\/cs7010.vk.me\/c540105\/v540105420\/16e96\/C1djz_APdsQ.jpg","width":790,"height":511,"text":"","created":1420008565,"access_key":"d58af84c6abcdea676"}},{"type":"page","page":{"pid":"50065784","gid":22822305,"title":"Темы ВКонтакте 2014","view_url":"http:\/\/m.vk.com\/page-22822305_50065784?api_view=97037b48b424a6c97ad38037b48e67"}}],"comments":{"count":0},"likes":{"count":15086},"reposts":{"count":1607}},{"id":103901,"from_id":-22822305,"to_id":-22822305,"date":1417511382,"post_type":"post","text":"Информационные технологии и программирование доступны абсолютно всем! Каждый может почувствовать себя создателем и реализовать идею с нуля.<br><br>С 4 по 12 декабря 2014 года пройдет беспрецедентная акция #ЧасКода, которая объединит всю страну! В каждой российской школе пройдет уникальный урок, которого еще никогда не было и который, надеемся, изменит ваше представление об ИТ и программировании.<br><br>Для начала ИТ-гуру — первые лица знаменитых ИТ-компаний — лично осветят все преимущества молодой и крайне перспективной индустрии, познакомят с невероятными возможностями, открывающимися в ней, и помогут найти свой успешный путь! Затем каждый сможет попробовать свои силы в программировании, выполнив увлекательные задания на одном из двух онлайн тренажеров, доступных на сайте.<br><br>Помните, программирование — это проще, чем кажется. Стоит только попробовать!<br>coderussia.ru<br><br>#coderussia@team","attachment":{"type":"video","video":{"vid":170902699,"owner_id":-22822305,"title":"Выбирай ИТ!","duration":175,"description":"Информационные технологии и программирование доступны абсолютно всем! <br>С 4 по 12 декабря 2014 года пройдет беспрецедентная акция #ЧасКода. В каждой российской школе пройдет уникальный урок, которого еще никогда не было и который изменит ваше представление о программировании. Присоединяйтесь и станьте частью успешного будущего!","date":1417459286,"views":28637,"image":"http:\/\/cs624217.vk.me\/u116873145\/video\/l_1202ac94.jpg","image_big":"http:\/\/cs624217.vk.me\/u116873145\/video\/l_1202ac94.jpg","image_small":"http:\/\/cs624217.vk.me\/u116873145\/video\/s_25ea7e06.jpg","access_key":"c40c8e341d2c3a0b18"}},"attachments":[{"type":"video","video":{"vid":170902699,"owner_id":-22822305,"title":"Выбирай ИТ!","duration":175,"description":"Информационные технологии и программирование доступны абсолютно всем! <br>С 4 по 12 декабря 2014 года пройдет беспрецедентная акция #ЧасКода. В каждой российской школе пройдет уникальный урок, которого еще никогда не было и который изменит ваше представление о программировании. Присоединяйтесь и станьте частью успешного будущего!","date":1417459286,"views":28637,"image":"http:\/\/cs624217.vk.me\/u116873145\/video\/l_1202ac94.jpg","image_big":"http:\/\/cs624217.vk.me\/u116873145\/video\/l_1202ac94.jpg","image_small":"http:\/\/cs624217.vk.me\/u116873145\/video\/s_25ea7e06.jpg","access_key":"c40c8e341d2c3a0b18"}}],"comments":{"count":0},"likes":{"count":19912},"reposts":{"count":1886}}]}';
		var groupResponse = '{"response":[{"gid":22822305,"name":"Команда ВКонтакте","screen_name":"team","is_closed":0,"type":"page","description":"Это официальная страница команды ВКонтакте. Здесь Вы можете узнать обо всех изменениях на сайте из первых рук.","photo":"http:\/\/cs623616.vk.me\/v623616034\/1c185\/uEITfF8iLT8.jpg","photo_medium":"http:\/\/cs623616.vk.me\/v623616034\/1c184\/MnbEYczHxSY.jpg","photo_big":"http:\/\/cs623616.vk.me\/v623616034\/1c183\/iYHejnzHARQ.jpg"}]}';


		beforeEach(module('vkGroupApp'));
		beforeEach(inject(function(_$httpBackend_, $rootScope, _$routeParams_, $controller) {
			$httpBackend = _$httpBackend_;
			$routeParams = _$routeParams_;
			$httpBackend.expectJSONP('https://api.vk.com/method/groups.getById?callback=JSON_CALLBACK&fields=description&group_id=22822305')
				.respond(groupResponse);
			$httpBackend.expectJSONP('https://api.vk.com/method/wall.get?callback=JSON_CALLBACK&count=20&filter=owner&owner_id=-22822305')
				.respond(groupDetailResponse);
			

			$routeParams.groupId = '22822305';
			scope = $rootScope.$new();
			ctrl = $controller('GroupDetailsCtrl', {$scope: scope});
		}));

		it('should create "posts" model with 5 posts fetched from xhr', function () {
			expect(scope.posts).toEqual([]);
			$httpBackend.flush();
			expect(scope.posts.length).toBe(5);
		});

		it('all posts should have the same group id', function () {
			$httpBackend.flush();
			var grId = -$routeParams.groupId;
			angular.forEach(scope.posts, function (value, key, arr) {
				expect(value.from_id).toBe(grId);
			});
		});
		it('should delete post', function () {
			$httpBackend.flush();
			var pId = 111725,
				grId = -22822305;
			scope.removePost(pId, grId, 0);
			expect(scope.posts.length).toBe(4);
			var localDeletedPosts = angular.fromJson(localStorage.getItem('deletedPosts'));
			expect(localDeletedPosts[grId]).not.toBeUndefined();
			expect(localDeletedPosts[grId]).toEqual([pId]);
			//expect(true).toBe(false);
		});
	});
	describe('Group List Controller', function () {
		describe('without query', function () {
			var scope, ctrl, $httpBackend, $routeParams;
			var response = '{"response":[111655,{"gid":22822305,"name":"Команда ВКонтакте","screen_name":"team","is_closed":0,"type":"page","photo":"http:\/\/cs623616.vk.me\/v623616034\/1c185\/uEITfF8iLT8.jpg","photo_medium":"http:\/\/cs623616.vk.me\/v623616034\/1c184\/MnbEYczHxSY.jpg","photo_big":"http:\/\/cs623616.vk.me\/v623616034\/1c183\/iYHejnzHARQ.jpg"},{"gid":27902394,"name":"ВКонтакте для Android","screen_name":"android_app","is_closed":0,"type":"group","photo":"http:\/\/cs403617.vk.me\/v403617492\/7e50\/IKlkRdVQprU.jpg","photo_medium":"http:\/\/cs403617.vk.me\/v403617492\/7e4f\/BKyzwnjs5-s.jpg","photo_big":"http:\/\/cs403617.vk.me\/v403617492\/7e4e\/T8kMvhT60bk.jpg"},{"gid":7278106,"name":"ВКонтакте для iPhone","screen_name":"iphone_app","is_closed":0,"type":"group","photo":"http:\/\/cs416628.vk.me\/v416628492\/8716\/StaPvhWh5gM.jpg","photo_medium":"http:\/\/cs416628.vk.me\/v416628492\/8715\/lsXcYHOs2u8.jpg","photo_big":"http:\/\/cs416628.vk.me\/v416628492\/8714\/C5JBHgM8fXk.jpg"},{"gid":22079806,"name":"Официальные страницы ВКонтакте","screen_name":"officialpages","is_closed":0,"type":"page","photo":"http:\/\/cs624819.vk.me\/v624819034\/cce6\/A8n0PYwziPU.jpg","photo_medium":"http:\/\/cs624819.vk.me\/v624819034\/cce5\/fVrC4nwEW6U.jpg","photo_big":"http:\/\/cs624819.vk.me\/v624819034\/cce4\/d68Mxbza620.jpg"},{"gid":35502680,"name":"ВКонтакте для Windows Phone","screen_name":"wp_app","is_closed":0,"type":"group","photo":"http:\/\/cs416628.vk.me\/v416628492\/8719\/ytFQHAL0WGE.jpg","photo_medium":"http:\/\/cs416628.vk.me\/v416628492\/8718\/ehdKJOi9_oQ.jpg","photo_big":"http:\/\/cs416628.vk.me\/v416628492\/8717\/rXwphSowBOs.jpg"}]}';

			beforeEach(module('vkGroupApp'));
			beforeEach(inject(function(_$httpBackend_, $rootScope, _$routeParams_, $controller) {
				$httpBackend = _$httpBackend_;
				$routeParams = _$routeParams_;
				$httpBackend.expectJSONP('https://api.vk.com/method/groups.search?count=5&q=%D0%92%D0%BA%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D0%B5')
					.respond(response);

				$routeParams = {};
				scope = $rootScope.$new();
				ctrl = $controller('GroupListCtrl', {$scope: scope});
			}));

			it('should not get groups if route params query is empty', function () {
				expect(scope.groups).toEqual([]);
				$httpBackend.verifyNoOutstandingRequest();
				expect(scope.groups).toEqual([]);
			});
		});	
		describe('with query', function () {
			var scope, ctrl, $httpBackend, $routeParams;
			var response = '{"response":[178649,{"gid":43195089,"name":"Armada Music","screen_name":"armadamusic","is_closed":0,"type":"page","photo":"http:\/\/cs620018.vk.me\/v620018384\/1312a\/EC6OYfYRmOA.jpg","photo_medium":"http:\/\/cs620018.vk.me\/v620018384\/13129\/QaLQ5cDbmvQ.jpg","photo_big":"http:\/\/cs620018.vk.me\/v620018384\/13127\/4B7fpDbrsB8.jpg"},{"gid":339767,"name":"A-ONE HIP-HOP MUSIC CHANNEL","screen_name":"a1tv","is_closed":0,"type":"group","photo":"http:\/\/cs623824.vk.me\/v623824923\/12f11\/V18RL78ZKrM.jpg","photo_medium":"http:\/\/cs623824.vk.me\/v623824923\/12f10\/Omgh9HvK7j8.jpg","photo_big":"http:\/\/cs623824.vk.me\/v623824923\/12f0f\/84DeEarz9Zg.jpg"},{"gid":39403313,"name":"M1 music channel","screen_name":"m1television","is_closed":0,"type":"group","photo":"http:\/\/cs625324.vk.me\/v625324527\/72ca\/d54_d5Tod9E.jpg","photo_medium":"http:\/\/cs625324.vk.me\/v625324527\/72c9\/BKL9-MPBQPc.jpg","photo_big":"http:\/\/cs625324.vk.me\/v625324527\/72c8\/8Q6PiQF8KEY.jpg"},{"gid":41265278,"name":"Velvet Music","screen_name":"velvet_music","is_closed":0,"type":"group","photo":"http:\/\/cs618920.vk.me\/v618920177\/171d5\/0mswZNyndCw.jpg","photo_medium":"http:\/\/cs618920.vk.me\/v618920177\/171d4\/x76pB25DU50.jpg","photo_big":"http:\/\/cs618920.vk.me\/v618920177\/171d3\/rgfBdihJ9MI.jpg"},{"gid":71427090,"name":"XD: Love Dance Music","screen_name":"gamexdance","is_closed":0,"type":"group","photo":"http:\/\/cs622118.vk.me\/v622118291\/b83c\/lSmuTvOjdIM.jpg","photo_medium":"http:\/\/cs622118.vk.me\/v622118291\/b83b\/YOIYSXdKmFM.jpg","photo_big":"http:\/\/cs622118.vk.me\/v622118291\/b83a\/-AAOcQmAHfE.jpg"}]}';
			beforeEach(module('vkGroupApp'));
			beforeEach(inject(function(_$httpBackend_, $rootScope, _$routeParams_, $controller) {
				$httpBackend = _$httpBackend_;
				$routeParams = _$routeParams_;
				$httpBackend.expectJSONP('https://api.vk.com/method/groups.search?access_token=fsfsdf43455345354&callback=JSON_CALLBACK&count=10&q=Music')
					.respond(response);

				localStorage.setItem('userInfo','{"id":"12739487","accessToken":"fsfsdf43455345354"}');
				$routeParams.query = 'Music';
				scope = $rootScope.$new();
				ctrl = $controller('GroupListCtrl', {$scope: scope});
			}));

			it('should create "groups" model with 5 groups fetched from xhr', function () {
				expect(scope.groups).toEqual([]);
				$httpBackend.flush();
				expect(scope.groups.length).toBe(5);
			});
		});	
	});
	describe('Auth Redirect Controller', function () {
		describe('success login', function () {
			var scope, ctrl, userInfo;

			beforeEach(module('vkGroupApp'));
			beforeEach(inject(function($injector) {
    			userInfo = $injector.get('userInfo');
			}));
			beforeEach(inject(function($rootScope, $controller, $location) {
				$location.url('http://localhost:8000/app/#/auth#access_token=54546jfsjfjskfjsd&expires_in=86400&user_id=8724047');
				
				scope = $rootScope.$new();
				ctrl = $controller('AuthRedirectCtrl', {$scope: scope});
			}));

			it('should set accessToken and userId', function () {
				expect(userInfo.id).toEqual('8724047');
				expect(userInfo.accessToken).toEqual('54546jfsjfjskfjsd');
			});
		});	
		describe('unsuccess login', function () {
			var scope, ctrl, userInfo;

			beforeEach(module('vkGroupApp'));
			beforeEach(inject(function($injector) {
    			userInfo = $injector.get('userInfo');
			}));
			beforeEach(inject(function($rootScope, $controller, $location) {
				$location.url('http://localhost:8000/app/#/auth#error=access_denied&error_description=The+user+or+authorization+server+denied+the+request');
				userInfo.id = null;
				userInfo.accessToken = null;
				scope = $rootScope.$new();
				ctrl = $controller('AuthRedirectCtrl', {$scope: scope});
			}));
			it('should not set accessToken and userId', function () {
				expect(userInfo.id).toEqual(null);
				expect(userInfo.accessToken).toEqual(null);
			});
		});
	});
});







