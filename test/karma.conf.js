module.exports = function(config){
  config.set({

    basePath : '../',

    files : [
      'app/bower_components/angular/angular.js',
      'app/bower_components/angular-route/angular-route.js',
      'app/bower_components/angular-animate/angular-animate.js',
      'app/bower_components/angular-mocks/angular-mocks.js',

      "app/app.js",
      "app/group-list/group-list.js",
      "app/group-details/group-details.js",
      "app/auth-redirect/auth-redirect.js",
      "app/group-service/group-service.js",
      "app/user-config/user-config.js",
      "app/nav-directive/nav-directive.js",
      "app/vk-api/vk-api.js",
      "app/vk-api/vk-api-groups.js",
      "app/vk-api/vk-api-video.js",
      "app/vk-api/vk-oauth.js",
      "app/filters/filters.js",
      "app/bower_components/jquery/dist/jquery.min.js",
      "app/bower_components/bootstrap/dist/js/bootstrap.min.js",

      'test/unit/**/*.js'
    ],

    exclude : [
      'app/bower_components/bootstrap/**/*.js',
      'app/bower_components/jquery/**/*.js'
    ],

    preprocessors : {
      "app/app.js" : 'coverage',
      "app/group-list/group-list.js" : 'coverage',
      "app/group-details/group-details.js" : 'coverage',
      "app/auth-redirect/auth-redirect.js" : 'coverage',
      "app/group-service/group-service.js" : 'coverage',
      "app/user-config/user-config.js" : 'coverage',
      "app/nav-directive/nav-directive.js" : 'coverage',
      "app/vk-api/vk-api.js" : 'coverage',
      "app/vk-api/vk-api-groups.js" : 'coverage',
      "app/vk-api/vk-oauth.js" : 'coverage',
      "app/filters/filters.js" : 'coverage'
    },
    /*reporters : ['coverage'],

    coverageReporter : {
      type : 'html',
      dir : 'coverage/'
    },*/

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-coverage'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};