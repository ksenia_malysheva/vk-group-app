'use strict';

angular.module('vkGroupApp.group-details', ['vkGroupApp.group-service', 'vkGroupApp.vk-api'])
.controller('GroupDetailsCtrl', [
	'$scope',
	'$routeParams',
	'groupInfo',
	'vkApiVideo',
	'$window',
	function ($scope, $routeParams, groupInfo, vkApiVideo, $window) {
		$scope.groupId = $routeParams.groupId;
		$scope.posts = [];
		$scope.group = {};
		var deletedPosts = {};
		var localStorageKey = 'deletedPosts';
		var localData = localStorage.getItem(localStorageKey);
		if(localData !== null) {
			angular.extend(deletedPosts, angular.fromJson(localData));
		}
		$scope.group = groupInfo.getGroupById($scope.groupId);
		$scope.posts = groupInfo.getGroupWall($scope.groupId);

		$scope.removePost = function (postId, ownerId, index) {
			if(!deletedPosts[ownerId]) {
				deletedPosts[ownerId] = [];
			}
			deletedPosts[ownerId].push(postId);
			localStorage.setItem(localStorageKey, angular.toJson(deletedPosts));

			$scope.posts.splice(index,1);
		};

		$scope.fetchVideo = function (videoId, ownerId){
			var p = vkApiVideo.getVideo(videoId, ownerId);
			p.success(function (data) {
				var videoUrl = data.response[1].player;
				$window.open(videoUrl);
			})
		};
}]);