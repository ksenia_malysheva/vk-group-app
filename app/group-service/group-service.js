'use strict';

angular.module('vkGroupApp.group-service', ['vkGroupApp.vk-api'])
	.service('groupInfo', ['vkApiGroups', function (vkApiGroups) {
		
		this.getGroups = function (query) {
			var groups = [];

			var p = vkApiGroups.search(query);
  			p.success(function (data) {
		  		if(!data.error) {
					var response = data.response;
		  			response.shift();
		  			angular.copy(response, groups);
		  		}
		  		else {
		  			alert('Error, please try again.');
		  		}
		  		
	  		})
		  	.error(function () {
		  		console.log('vk getGroups error');
		  	});
		  	return groups;
		};
		this.getGroupById = function (gid) {
			var group = {};

			var p = vkApiGroups.getById(gid);
  			p.success(function (data) {
		  		if(!data.error) {
					var response = data.response[0];
		  			angular.copy(response, group);
		  		}
		  		else {
		  			alert('Error, please try again.');
		  		}
		  		
	  		})
		  	.error(function () {
		  		console.log('vk getGroups error');
		  	});
		  	return group;
		};
		var filterDeletedPosts = function (inputArr, gid) {
			var deletedPosts = {};
			var localStorageKey = 'deletedPosts';
			var localData = localStorage.getItem(localStorageKey);
			if(localData !== null) {
				angular.extend(deletedPosts, angular.fromJson(localData));
			}
			if(deletedPosts[-gid]) {
				for(var i = 0; i < deletedPosts[-gid].length; i++) {
					for(var j = 0; j < inputArr.length; j++) {
						if(inputArr[j].id === deletedPosts[-gid][i]) {
							inputArr.splice(j,1);
						}
					}
				}
			}
		};
		this.getGroupWall = function (gid) {
			var posts = [];

			var p = vkApiGroups.getWall(gid);
  			p.success(function (data) {
		  		if(!data.error) {
		  			var response = data.response;
		  			response.shift();
		  			filterDeletedPosts(response, gid);
		  			angular.copy(response, posts);
		  		}
		  		else {
		  			alert('Error, please try again.');
		  		}
	  		})
		  	.error(function () {
		  		console.log('vk getGroupWall error');
		  	});
		  	return posts;
		}
	}]);