'use strict';

/* App Module */
var vkGroupApp = angular.module('vkGroupApp', [
	'ngRoute', 
  'ngAnimate',
  'vkGroupApp.group-list',
  'vkGroupApp.group-details',
  'vkGroupApp.auth-redirect',
  'vkGroupApp.nav-directive',
	'vkGroupApp.group-service',
  'vkGroupApp.user-config',
  'vkGroupApp.vk-api',
	'vkGroupApp.filters'
]);
vkGroupApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'group-list/group-list.html',
        controller: 'GroupListCtrl'
      }).
  	  when('/auth', {
  		template: '',
  		controller: 'AuthRedirectCtrl'
  	  }).
  	  when('/group/:groupId', {
  	  	templateUrl: 'group-details/group-details.html',
        controller: 'GroupDetailsCtrl'
  	  }).
      otherwise({
        redirectTo: '/'
      });
  }]);