'use strict';

angular.module('vkGroupApp.auth-redirect', ['vkGroupApp.user-config'])
.controller('AuthRedirectCtrl', [
	'$location',
	'userInfo',

	function ($location, userInfo) {
		var locationPath = $location.absUrl();
		if(locationPath.indexOf('error') === -1) {
			var accessToken = '', userId = '';
			var matches = locationPath.match(/access_token=(.*?)&/i);
			if(matches.length > 1) {
				accessToken = matches[1];
			}
			userId = locationPath.substring(locationPath.lastIndexOf('=')+1);		
			userInfo.accessToken = accessToken;
			userInfo.id = userId;
		}
		$location.url('/');
}]);
