'use strict';

angular.module('vkGroupApp.group-list', ['vkGroupApp.group-service'])
.controller('GroupListCtrl', [
	'$scope', 
	'$routeParams',
	'groupInfo', 

	function($scope, $routeParams, groupInfo) {
	  $scope.groups = [];

	  if($routeParams.query) {
	  	$scope.query = $routeParams.query;
	  	$scope.groups = groupInfo.getGroups($scope.query);
	  }
}]);