'use strict';

angular.module('vkGroupApp.nav-directive', ['vkGroupApp.vk-api', 'vkGroupApp.user-config'])
.directive('navMenu', [
	'vkApiOAuth',
	'userInfo',
	function (vkApiOAuth, userInfo) {
		return {
			replace: true,
			restrict: 'E',
			templateUrl: 'nav-directive/nav-directive.html',
			controller: ['$scope', '$location', function ($scope, $location) {
				$scope.getGroups = function (query) {
			  	query = query || 'Вконтакте';
			  	$location.url('/?query=' + query);
			  };
			  $scope.login = function () {
			  	vkApiOAuth.redirectToVKAuth();
			  };
			  $scope.logout = function () {
			  	userInfo.id = null;
			  	userInfo.accessToken = null;
			  	$scope.loggedIn = false;
			  	$location.url('/');
			  };
			  $scope.loggedIn = userInfo.accessToken ? true : false;
			}]
		};
}]);