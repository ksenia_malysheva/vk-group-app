'use strict';

angular.module('vkGroupApp.vk-api')
.service('vkApiOAuth', [
	'$location', 
	'$window', 

	function ($location, $window) {
		this.redirectToVKAuth = function () {
			var redirectUriEncoded = encodeURIComponent($location.absUrl() + 'auth');
			var vkAuthUrl;
			var req = {
	 			url: 'https://oauth.vk.com/authorize',
	 			params: {
	 				client_id: '?client_id=4781033',
	 				scope: '&scope=video',
	 				redirect_uri: '&redirect_uri=' + redirectUriEncoded,
	 				display: '&display=page',
	 				v: '&v=5.28',
	 				response_type: '&response_type=token',
	 			}
			};
			vkAuthUrl = req.url + req.params.client_id + req.params.scope + req.params.redirect_uri + req.params.display + req.params.v + req.params.response_type;
			$window.location.href = vkAuthUrl;
		};
}]);