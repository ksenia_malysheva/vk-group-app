'use strict';

angular.module('vkGroupApp.vk-api')
.service('vkApiGroups', [
	'$http', 
	'userInfo', 
	function ($http, userInfo) {
		this.search = function (keywords) {
			var req = {
				method: 'jsonp',
			  	url: 'https://api.vk.com/method/groups.search',
			  	params: {
			  		callback: 'JSON_CALLBACK',
			  		q: keywords,
			  		count: 10,
			  		access_token: userInfo.accessToken || ''
			  	}
			};
			return $http(req);
		};
		this.getById = function (gid) {
			var req = {
				method: 'jsonp',
			  	url: 'https://api.vk.com/method/groups.getById',
			  	params: {
			  		callback: 'JSON_CALLBACK',
			  		group_id: gid,
			  		fields: 'description'
			  	}
			};
			return $http(req);
		};
		this.getWall = function (gid) {
			var req = {
				method: 'jsonp',
			  	url: 'https://api.vk.com/method/wall.get',
			  	params: {
			  		callback: 'JSON_CALLBACK',
			  		owner_id: -gid,
			  		count: 20,
					filter: 'owner'
			  	}
			};
			return $http(req);
		};
}]);