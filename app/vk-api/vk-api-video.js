'use strict';

angular.module('vkGroupApp.vk-api')
.service('vkApiVideo', ['userInfo', '$http', function (userInfo, $http) {
	this.getVideo = function (videoId, ownerId) {
			var req = {
				method: 'jsonp',
			  	url: 'https://api.vk.com/method/video.get',
			  	params: {
			  		callback: 'JSON_CALLBACK',
			  		owner_id: ownerId,
			  		videos: ownerId + '_' + videoId,
			  		count: 1,
			  		access_token: userInfo.accessToken
			  	}
			};
			return $http(req);
	};
}]);