'use strict';

angular.module('vkGroupApp.filters', [])
.filter('htmlToPlaintext', function() {
    return function(text) {
      return String(text).replace(/<[^>]+>/gm, ' ');
    }
  });