'use strict';

angular.module('vkGroupApp.user-config', [])
.factory('userInfo', [
	'$rootScope', 
	function($rootScope) {
		var localStorageKey = "userInfo";
		var user = {
			id: null,
			accessToken: null
		};
		var localData = localStorage.getItem(localStorageKey);
		if (localData !== null){
			angular.extend(user, angular.fromJson(localData)); //security?
		}
		$rootScope.$watch(
			function () {
				return user;
			},
			function (newValue) {
				localStorage.setItem(localStorageKey, angular.toJson(newValue));
			}, true);
		return user;
}]);